#include <gtest/gtest.h>

// Returns n! (the factorial of n).  For negative n, n! is defined to be 1.
static int Factorial(int n) {
  int result = 1;
  for (int i = 1; i <= n; i++) {
    result *= i;
  }
  return result;
}

TEST(Factorial, Valid) {
    EXPECT_EQ(24, Factorial(4));
    EXPECT_EQ(1, Factorial(1));
    EXPECT_EQ(5040, Factorial(7));
}
